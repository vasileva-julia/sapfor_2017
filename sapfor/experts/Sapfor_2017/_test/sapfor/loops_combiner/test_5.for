      program loops_combiner_test
      implicit none
      
      integer :: t1, t2
      integer :: i, j, i1, j1, k
      real :: mas(10, 20)


! должны быть объединены с разворотом первого гнезда:
      do i = 1, 10
         do j = 1, 20, 1
            mas(i, j) = i + j
         enddo
      enddo
      do i = 10, 1, -1
         do j = 20, 1, -1
            mas(i, j) = mas(i, j) + i * j
         enddo
      enddo

! для разделения циклов:
      k = 0

! переменная i1 должна быть инициализирована перед объединённым циклом
! переменная j1 - после, т.к. используется в первом цикле:
      do i = 1, 10
         do j = 1, 20
            j1 = i + j
            mas(i, j) = j1
         enddo
      enddo
      do i1 = 1, 10
         do j1 = 1, 20
            mas(i1, j1) = mas(i1, j1) + i1 * j1
         enddo
      enddo
      
! для разделения циклов:
      k = 0

! переменная i во втором цикле наследует значение от первого,
! должны быть заменена на соответствующее выражение:
      do i = 1, 10
         do j = 1, 20
            mas(i, j) = i + j
         enddo
      enddo
      do i1 = 1, 10
         do j1 = 1, 20
            mas(i1, j1) = mas(i1, j1) + i1 * j1 - i
         enddo
      enddo

! для разделения циклов:
      k = 0

! переменная i во втором цикле наследует значение от первого,
! должны быть переименована и проинициализирована:
      do i = 1, 10
         do j = 1, 20
            mas(i, j) = i + j
         enddo
      enddo
      do i1 = 1, 10
         do j1 = 1, 20
            k = k + i
            i = j1 / i1
         enddo
      enddo

! для разделения циклов:
      k = 0

! циклы нельзя объединить из-за неприватной для обоих переменной t2:
      t2 = 0
      do i = 1, 10
         do j = 1, 20, 1
            t2 = t2 + i * j
         enddo
      enddo
      do i = 1, 10
         do j = 1, 20
            t1 = i + j - t2
         enddo
      enddo 
      
      end       
       

      program loops_combiner_test
      implicit none
      parameter (l = 16,m = 6)
      real :: a(l),b(l),c(l)


! should be combined by it1-it2 dimension only:
      do it1 = 1,m
         do k1 = 1,l
               a(k1) = it1 + k1
         enddo
      enddo 

      do it2 = 1,m
         do k2 = 1,l - 1
            a(k2) = it2 * k2
         enddo
      enddo
      
! should be combined by it1-it2, k1-k2 dimensions only:
      do it1 = 1,m - 1
         do k1 = 1,l
            do p1 = 1,l,2
               do j1 = 1, l
                  a(k1) = j1 * p1 - it1
               enddo
            enddo
         enddo
      enddo
	  
      do it2 = 1,m - 1
         do k2 = 1,l
            do p2 = 1,l
               a(k2) = it2 + k2 * p2
            enddo
         enddo
      enddo

      end


cmake_minimum_required(VERSION 3.0)

set(project Sapfor)
if (TARGET ${project})
    return()
endif ()
project(${project})
message("processing ${project}")
#include(CTest)

add_definitions("-D SYS5")
add_definitions("-D YYDEBUG")

#set(CMAKE_CXX_STANDARD 11)

set(fdvm_include ../../../dvm/fdvm/trunk/include)
set(sage_include_1 ../../../dvm/fdvm/trunk/Sage/lib/include)
set(sage_include_2 ../../../dvm/fdvm/trunk/Sage/h/)
set(libdb_sources ../../../dvm/fdvm/trunk/Sage/lib/oldsrc)
set(sage_sources ../../../dvm/fdvm/trunk/Sage/lib/newsrc)
set(sagepp_sources ../../../dvm/fdvm/trunk/Sage/Sage++)
set(parser_sources ../../../dvm/fdvm/trunk/parser)
set(pppa_sources ../../../dvm/tools/pppa/trunk/src)
set(zlib_sources ../../../dvm/tools/Zlib)

# Read pathes to external sapfor directories
if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/paths.txt")
    message("Found paths.txt, using custom paths.")
    FILE(STRINGS ./paths.txt SAPFOR_PATHS)
else ()
    message("Not found paths.txt, using default paths.")
    FILE(STRINGS ./paths.default.txt SAPFOR_PATHS)
endif ()
  
foreach (NameAndValue ${SAPFOR_PATHS})
    # Strip leading spaces
    string(REGEX REPLACE "^[ ]+" "" NameAndValue ${NameAndValue})
    # Find variable name
    string(REGEX MATCH "^[^=]+" Name ${NameAndValue})
    # Find the value
    string(REPLACE "${Name}=" "" Value ${NameAndValue})
    # Set the variable, note the ../ because we are deeper than the file
    set(${Name} "${Value}")
    message("Using ${Name} ${${Name}}")
endforeach ()

include_directories(_src)
#Sage lib includes
include_directories(${fdvm_include})
include_directories(${sage_include_1})
include_directories(${sage_include_2})
#Zlib includes
include_directories(${zlib_sources}/include)
#PPPA includes
include_directories(${pppa_sources})

set(PR_PARAM _src/ProjectParameters/projectParameters.cpp
			 _src/ProjectParameters/projectParameters.h)

set(GR_LAYOUT _src/VisualizerCalls/graphLayout/algebra.cpp
		_src/VisualizerCalls/graphLayout/algebra.hpp
		_src/VisualizerCalls/graphLayout/fruchterman_reingold.cpp
		_src/VisualizerCalls/graphLayout/fruchterman_reingold.hpp
		_src/VisualizerCalls/graphLayout/kamada_kawai.cpp
		_src/VisualizerCalls/graphLayout/kamada_kawai.hpp
		_src/VisualizerCalls/graphLayout/layout.cpp
		_src/VisualizerCalls/graphLayout/layout.hpp
		_src/VisualizerCalls/graphLayout/nodesoup.cpp
		_src/VisualizerCalls/graphLayout/nodesoup.hpp)
	
set(VS_CALLS _src/VisualizerCalls/get_information.cpp
		_src/VisualizerCalls/get_information.h
		_src/VisualizerCalls/SendMessage.cpp
		_src/VisualizerCalls/SendMessage.h
		_src/VisualizerCalls/BuildGraph.cpp
		_src/VisualizerCalls/BuildGraph.h)
	
set(VERIF _src/VerificationCode/CorrectVarDecl.cpp
        _src/VerificationCode/IncludeChecker.cpp
        _src/VerificationCode/StructureChecker.cpp
        _src/VerificationCode/VerifySageStructures.cpp
		_src/VerificationCode/verifications.h)

set(UTILS _src/Utils/AstWrapper.h
		_src/Utils/BoostStackTrace.cpp
		_src/Utils/CommonBlock.h
		_src/Utils/DefUseList.h
		_src/Utils/errors.h
		_src/Utils/leak_detector.h
		_src/Utils/PassManager.h
		_src/Utils/RationalNum.cpp
		_src/Utils/RationalNum.h
        _src/Utils/SgUtils.cpp
		_src/Utils/SgUtils.h
        _src/Utils/types.h
        _src/Utils/utils.cpp
		_src/Utils/utils.h
		_src/Utils/version.h)
		
set(OMEGA _src/SageAnalysisTool/OmegaForSage/add-assert.cpp
        _src/SageAnalysisTool/OmegaForSage/affine.cpp
        _src/SageAnalysisTool/OmegaForSage/cover.cpp
        _src/SageAnalysisTool/OmegaForSage/ddomega-build.cpp
        _src/SageAnalysisTool/OmegaForSage/ddomega-use.cpp
        _src/SageAnalysisTool/OmegaForSage/ddomega.cpp
        _src/SageAnalysisTool/OmegaForSage/debug.cpp
        _src/SageAnalysisTool/OmegaForSage/ip.cpp
        _src/SageAnalysisTool/OmegaForSage/kill.cpp
        _src/SageAnalysisTool/OmegaForSage/refine.cpp
        _src/SageAnalysisTool/OmegaForSage/sagedriver.cpp
        _src/SageAnalysisTool/annotationDriver.cpp
        _src/SageAnalysisTool/arrayRef.cpp
        _src/SageAnalysisTool/computeInducVar.cpp
        _src/SageAnalysisTool/constanteProp.cpp
        _src/SageAnalysisTool/constanteSet.h
        _src/SageAnalysisTool/controlFlow.cpp
        _src/SageAnalysisTool/defUse.cpp
        _src/SageAnalysisTool/dependence.cpp
        _src/SageAnalysisTool/depGraph.cpp
        _src/SageAnalysisTool/depInterface.cpp
        _src/SageAnalysisTool/depInterfaceExt.h
        _src/SageAnalysisTool/flowAnalysis.cpp
        _src/SageAnalysisTool/inducVar.h
        _src/SageAnalysisTool/intrinsic.cpp
        _src/SageAnalysisTool/invariant.cpp
        _src/SageAnalysisTool/loopTransform.cpp
        _src/SageAnalysisTool/set.cpp)

set(PRIV _src/PrivateAnalyzer/private_analyzer.cpp
		_src/PrivateAnalyzer/private_analyzer.h)

set(FDVM ${fdvm_sources}/acc.cpp
		 ${fdvm_sources}/acc_across.cpp
		 ${fdvm_sources}/acc_across_analyzer.cpp
	     ${fdvm_sources}/acc_analyzer.cpp
         ${fdvm_sources}/acc_data.cpp
		 ${fdvm_sources}/acc_f2c.cpp
		 ${fdvm_sources}/acc_f2c_handlers.cpp
		 ${fdvm_sources}/acc_rtc.cpp
		 ${fdvm_sources}/acc_rtc.cpp
         ${fdvm_sources}/acc_utilities.cpp
		 ${fdvm_sources}/aks_analyzeLoops.cpp
		 ${fdvm_sources}/aks_structs.cpp
		 ${fdvm_sources}/checkpoint.cpp
		 ${fdvm_sources}/debug.cpp
		 ${fdvm_sources}/dvm.cpp
         ${fdvm_sources}/calls.cpp
		 ${fdvm_sources}/funcall.cpp
		 ${fdvm_sources}/help.cpp
		 ${fdvm_sources}/hpf.cpp
		 ${fdvm_sources}/io.cpp
		 ${fdvm_sources}/omp.cpp
		 ${fdvm_sources}/ompdebug.cpp
		 ${fdvm_sources}/parloop.cpp
		 ${fdvm_sources}/stmt.cpp)
		
set(PARALLEL_REG _src/ParallelizationRegions/ParRegions.cpp
		_src/ParallelizationRegions/ParRegions.h
		_src/ParallelizationRegions/ParRegions_func.h
		_src/ParallelizationRegions/expand_extract_reg.cpp
		_src/ParallelizationRegions/expand_extract_reg.h
		_src/ParallelizationRegions/resolve_par_reg_conflicts.cpp
		_src/ParallelizationRegions/resolve_par_reg_conflicts.h)
	
set(TR_CP _src/Transformations/checkpoints.cpp
		_src/Transformations/checkpoints.h)
set(TR_VECTOR _src/Transformations/array_assign_to_loop.cpp
		_src/Transformations/array_assign_to_loop.h)
set(TR_ENDDO_LOOP _src/Transformations/enddo_loop_converter.cpp
		_src/Transformations/enddo_loop_converter.h)
set(TR_LOOP_NEST _src/Transformations/loop_transform.cpp
		_src/Transformations/loop_transform.h)
set(TR_LOOP_COMB _src/Transformations/loops_combiner.cpp
		_src/Transformations/loops_combiner.h)
set(TR_LOOP_SPLIT _src/Transformations/loops_splitter.cpp
		_src/Transformations/loops_splitter.h)
set(TR_PRIV_BR _src/Transformations/private_arrays_resizing.cpp
        _src/Transformations/private_arrays_resizing.h)
set(TR_SWAP_ARR_DIMS _src/Transformations/swap_array_dims.cpp
		_src/Transformations/swap_array_dims.h)
set(TR_FUNC_DUP _src/Transformations/uniq_call_chain_dup.cpp
		_src/Transformations/uniq_call_chain_dup.h)
set(TR_FUNC_PURE _src/Transformations/function_purifying.cpp
		_src/Transformations/function_purifying.h)		
set(TRANSFORMS 
        ${TR_CP} 
		${TR_VECTOR}
		${TR_ENDDO_LOOP}
		${TR_LOOP_NEST}
		${TR_LOOP_COMB} 
		${TR_LOOP_SPLIT}
		${TR_PRIV_BR}
		${TR_SWAP_ARR_DIMS} 
		${TR_FUNC_DUP} 
		${TR_FUNC_PURE})
		
set(CFG _src/CFGraph/IR.cpp
		_src/CFGraph/IR.h
		_src/CFGraph/CFGraph.cpp
		_src/CFGraph/CFGraph.h)
		
set(CREATE_INTER_T	_src/CreateInterTree/CreateInterTree.cpp
		_src/CreateInterTree/CreateInterTree.h)

set(DIRA  _src/DirectiveProcessing/DirectiveAnalyzer.cpp
		_src/DirectiveProcessing/DirectiveAnalyzer.h
		_src/DirectiveProcessing/directive_creator.cpp
		_src/DirectiveProcessing/directive_creator_base.cpp
		_src/DirectiveProcessing/directive_creator.h
		_src/DirectiveProcessing/directive_parser.cpp
		_src/DirectiveProcessing/directive_parser.h
        _src/DirectiveProcessing/insert_directive.cpp
		_src/DirectiveProcessing/insert_directive.h
		_src/DirectiveProcessing/remote_access.cpp
		_src/DirectiveProcessing/remote_access_base.cpp
		_src/DirectiveProcessing/remote_access.h
		_src/DirectiveProcessing/shadow.cpp
		_src/DirectiveProcessing/shadow.h
		_src/DirectiveProcessing/spf_directive_preproc.cpp)
		
set(DISTR _src/Distribution/Array.cpp
        _src/Distribution/Array.h
        _src/Distribution/Arrays.h
        _src/Distribution/CreateDistributionDirs.cpp
		_src/Distribution/CreateDistributionDirs.h
        _src/Distribution/Cycle.cpp
		_src/Distribution/Cycle.h
        _src/Distribution/Distribution.cpp
		_src/Distribution/Distribution.h
        _src/Distribution/DvmhDirective.cpp
		_src/Distribution/DvmhDirective.h
		_src/Distribution/DvmhDirective_func.h
        _src/Distribution/DvmhDirectiveBase.cpp
		_src/Distribution/DvmhDirectiveBase.h
        _src/Distribution/GraphCSR.cpp
		_src/Distribution/GraphCSR.h)
		
set(DVMH_REG _src/DvmhRegions/DvmhRegionInserter.cpp
		_src/DvmhRegions/DvmhRegionInserter.h
		_src/DvmhRegions/RegionsMerger.cpp
		_src/DvmhRegions/RegionsMerger.h
		_src/DvmhRegions/ReadWriteAnalyzer.cpp
		_src/DvmhRegions/ReadWriteAnalyzer.h
		_src/DvmhRegions/LoopChecker.cpp
		_src/DvmhRegions/LoopChecker.h
		_src/DvmhRegions/DvmhRegion.cpp
		_src/DvmhRegions/DvmhRegion.h
		_src/DvmhRegions/VarUsages.cpp
		_src/DvmhRegions/VarUsages.h
		_src/DvmhRegions/TypedSymbol.cpp
		_src/DvmhRegions/TypedSymbol.h)
		
set(DYNA _src/DynamicAnalysis/createParallelRegions.cpp
		_src/DynamicAnalysis/createParallelRegions.h
		_src/DynamicAnalysis/gcov_info.cpp
		_src/DynamicAnalysis/gcov_info.h
		_src/DynamicAnalysis/gCov_parser.cpp
		_src/DynamicAnalysis/gCov_parser_func.h
		_src/DynamicAnalysis/codeEvaluation.cpp
		_src/DynamicAnalysis/codeEvaluation.h)

set(EXPR_TRANSFORM _src/ExpressionTransform/control_flow_graph_part.cpp
		_src/ExpressionTransform/expr_transform.cpp
		_src/ExpressionTransform/expr_transform.h)
		
set(GR_CALL _src/GraphCall/graph_calls.cpp
		_src/GraphCall/graph_calls.h
		_src/GraphCall/graph_calls_base.cpp
		_src/GraphCall/graph_calls_func.h)
		
set(GR_LOOP _src/GraphLoop/graph_loops_base.cpp
        _src/GraphLoop/graph_loops.cpp
		_src/GraphLoop/graph_loops.h
		_src/GraphLoop/graph_loops_func.h)
		
set(INLINER _src/Inliner/inliner.cpp
		_src/Inliner/inliner.h)
		
set(LOOP_ANALYZER  _src/LoopAnalyzer/allocations_prepoc.cpp
        _src/LoopAnalyzer/dep_analyzer.cpp        
        _src/LoopAnalyzer/loop_analyzer.cpp
		_src/LoopAnalyzer/loop_analyzer.h)

set(RENAME_SYMBOLS  _src/RenameSymbols/rename_symbols.cpp
					_src/RenameSymbols/rename_symbols.h)
		

set(MAIN _src/Sapfor.cpp
		_src/Sapfor.h
		_src/SapforData.h)

set(PREDICTOR _src/Predictor/PredictScheme.cpp
              _src/Predictor/PredictScheme.h)

set(PARSER  ${parser_sources}/cftn.c
			${parser_sources}/errors.c
			${parser_sources}/gram1.tab.c
			${parser_sources}/hash.c
			${parser_sources}/init.c
			${parser_sources}/lexfdvm.c
			${parser_sources}/lists.c
			${parser_sources}/low_hpf.c
			${parser_sources}/misc.c
			${parser_sources}/stat.c
			${parser_sources}/sym.c
			${parser_sources}/types.c
			${parser_sources}/unparse_hpf.c)

set(PPPA    ${pppa_sources}/inter.cpp
			${pppa_sources}/potensyn.cpp
			${pppa_sources}/stat.cpp
			${pppa_sources}/statfile.cpp
			${pppa_sources}/statinter.cpp
			${pppa_sources}/statlist.cpp
			${pppa_sources}/statprintf.cpp
			${pppa_sources}/statread.cpp
			${pppa_sources}/treeinter.cpp  
			
			${pppa_sources}/bool.h
			${pppa_sources}/dvmh_stat.h
			${pppa_sources}/inter.h
			${pppa_sources}/potensyn.h
			${pppa_sources}/statist.h
			${pppa_sources}/statlist.h
			${pppa_sources}/statprintf.h
			${pppa_sources}/statread.h
			${pppa_sources}/strall.h
			${pppa_sources}/sysstat.h
			${pppa_sources}/treeinter.h
			${pppa_sources}/ver.h
			${pppa_sources}/statinter.h
			${pppa_sources}/json.hpp)
			
set(ZLIB    ${zlib_sources}/src/adler32.c
			${zlib_sources}/src/compress.c
			${zlib_sources}/src/crc32.c
			${zlib_sources}/src/deflate.c
			${zlib_sources}/src/gzio.c
			${zlib_sources}/src/infblock.c
			${zlib_sources}/src/infcodes.c
			${zlib_sources}/src/inffast.c
			${zlib_sources}/src/inflate.c
			${zlib_sources}/src/inftrees.c
			${zlib_sources}/src/infutil.c
			${zlib_sources}/src/trees.c
			${zlib_sources}/src/uncompr.c
			${zlib_sources}/src/zutil.c)

set(SOURCE_EXE		
	${CFG}
	${CREATE_INTER_T}
	${DIRA}
	${DISTR}
	${DVMH_REG}
	${DYNA}
	${EXPR_TRANSFORM}
	${GR_CALL}
	${GR_LOOP}
	${INLINER}
	${LOOP_ANALYZER}
	${RENAME_SYMBOLS}
	${TRANSFORMS}
	${PARALLEL_REG}
	${PRIV}
	${FDVM}
	${OMEGA}
	${UTILS}
	${VERIF}
	${VS_CALLS}
	${MAIN}
	${PREDICTOR}
	${PARSER}
	${PPPA}
	${ZLIB}
	${GR_LAYOUT}
	${PR_PARAM})

add_executable(Sapfor_F ${SOURCE_EXE})
source_group (CFGraph FILES ${CFG})

source_group (Transformations\\ExpressionSubstitution FILES ${EXPR_TRANSFORM})
source_group (Transformations\\CheckPoints FILES ${TR_CP})
source_group (Transformations\\LoopEndDoConverter FILES ${TR_ENDDO_LOOP})
source_group (Transformations\\LoopNesting FILES ${TR_LOOP_NEST})
source_group (Transformations\\LoopCombining FILES ${TR_LOOP_COMB})
source_group (Transformations\\LoopSplitting FILES ${TR_LOOP_SPLIT})
source_group (Transformations\\FunctionDuplication FILES ${TR_FUNC_DUP})
source_group (Transformations\\FunctionInlining FILES ${INLINER})
source_group (Transformations\\FunctionPurifying FILES ${TR_FUNC_PURE})
source_group (Transformations\\ArrayDimsSwapping FILES ${TR_SWAP_ARR_DIMS})
source_group (Transformations\\PrivateArrayResizing FILES ${TR_PRIV_BR})
source_group (Transformations\\VectorAssignToLoop FILES ${TR_VECTOR})
source_group (Transformations\\RenameSymbols FILES ${RENAME_SYMBOLS})


source_group (CreateIntervals FILES ${CREATE_INTER_T})
source_group (DirectiveProcessing FILES ${DIRA})
source_group (Distribution FILES ${DISTR})
source_group (DvmhRegions FILES ${DVMH_REG})
source_group (DynamicAnalysis FILES ${DYNA})
source_group (GraphCall FILES ${GR_CALL})
source_group (GraphLoop FILES ${GR_LOOP})
source_group (LoopAnalyzer FILES ${LOOP_ANALYZER})
source_group (ParallelizationRegions FILES ${PARALLEL_REG})
source_group (PrivateAnalyzer FILES ${PRIV})	  
source_group (FDVM_Compiler FILES ${FDVM})
source_group (SageExtension FILES ${OMEGA})
source_group (Utils FILES ${UTILS})
source_group (VerificationCode FILES ${VERIF})	     
source_group (ProjectParameters FILES ${PR_PARAM})

source_group (VisualizerCalls FILES ${VS_CALLS})
source_group (VisualizerCalls\\GraphLayout FILES ${GR_LAYOUT})

source_group (_SapforMain FILES ${MAIN})
source_group (Predictor FILES ${PREDICTOR})
source_group (Parser FILES ${PARSER})
source_group (PPPA\\PPPA FILES ${PPPA})
source_group (PPPA\\ZLib FILES ${ZLIB})
   
if (MSVC_IDE)   
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP")
else()
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
   set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2")
endif()

add_subdirectory(FDVM)

add_definitions("-D __SPF")
add_definitions("-D _CRT_SECURE_NO_WARNINGS")
add_definitions("-D _CRT_NON_CONFORMING_SWPRINTFS")

add_subdirectory(SageOldSrc)
add_subdirectory(SageNewSrc)
add_subdirectory(SageLib)
add_subdirectory(Parser)

add_definitions("-D __SPF_BUILT_IN_FDVM")
add_definitions("-D __SPF_BUILT_IN_PARSER")
add_definitions("-D __SPF_BUILT_IN_PPPA")

if (WIN32)
	target_link_libraries(Sapfor_F SageNewSrc SageLib SageOldSrc)
elseif(UNIX)
	target_link_libraries(Sapfor_F SageNewSrc SageLib SageOldSrc pthread)
endif()

#install(TARGETS <name.exe/dll>
#    LIBRARY DESTINATION <name_f>
#	RUNTIME DESTINATION <name_f>)

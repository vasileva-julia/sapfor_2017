#pragma once

#include "Utils/SgUtils.h"

#include "string"
#include "map"

void runRenameSymbolsByFiles(SgFile* file, SgProject* project);
void runRenameSymbols(SgProject* project, const std::map<std::string, CommonBlock*>& commonBlocks);
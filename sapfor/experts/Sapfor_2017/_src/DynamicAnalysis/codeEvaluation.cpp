#include "codeEvaluation.h"

using std::vector;
using std::map;
using std::set;
using std::tuple;
using std::string;
using std::stack;
using std::pair;


const int DEFAULT_ITERATIONS = 10;

map<string, CodeEval*> funcEval; // 
map<string, double> funcsEval; // for gcov
map<string, map<int, double>> lineEval;  // base

static const OperationCost operationCost;


//
static FuncInfo* findMain(const map<string, vector<FuncInfo*>>& allFuncInfo);
static void loopsToMap(const map<string, vector<LoopGraph*>>& loopGraph, map<string, map<int, LoopGraph*>>& loopsByLine);
static void callsToMap(const vector<pair<string, int>>& detailCallsFrom, map<int, vector<string>>& funcsMap);
static void cleanChildList(CodeEval* val);
static void cleanFuncEval();
// Both
static void goThroughExpressions(SgExpression* expr, double& T);
// Static
// 1
static void goThroughFunction(SgStatement* st, CodeEval* val);
// 2
static void calculateEval(CodeEval* val, const map<int, LoopGraph*>& LoopsByLine, const map<int, vector<string>>& callsFrom);
static void goThroughFunctions(FuncInfo* f, const map<string, map<int, LoopGraph*>>& LoopsByLine);
// Gcov
// 2
static double finalFuncEval(FuncInfo* f, const map<int, Gcov_info >& gLineInfo);
static void calculateFuncs(FuncInfo* f, const map<string, map<int, Gcov_info>>& gCovInfo);
static void calculateLoops(const map<string, vector<LoopGraph*>>& loopGraph, map<string, map<int, LoopGraph*>>& loopsByLine);
static void calculateLoopsInOneFile(const vector<LoopGraph*>& loopGraphInfo, map<int, LoopGraph*>& loopsByLine);
// print
static void printLoopsEvalFromOneFile(vector<LoopGraph*>& loops);


static FuncInfo* findMain(const map<string, vector<FuncInfo*>>& allFuncInfo)
{
    for (const auto& item : allFuncInfo)
        for (auto elem : item.second)
            if (elem->isMain)
                return elem;
    return nullptr;
}


static void loopsToMap(const map<string, vector<LoopGraph*>>& loopGraph, map<string, map<int, LoopGraph*>>& loopsByLine)
{
    for (const auto& item : loopGraph)
        createMapLoopGraph(item.second, loopsByLine[item.first]);
}


static void cleanChildList(CodeEval* val) {
    if (val != nullptr)
    {
        for (auto child : val->childrenList)
            cleanChildList(child);
        if (val->variant == IF_NODE || val->variant == ELSEIF_NODE || val->variant == CONTROL_END) {
            IfEval* ifVal = (IfEval*)val;
            cleanChildList(ifVal->falseBody);
        }
        delete val;
    }
}


static void cleanFuncEval()
{
    for (auto item : funcEval)
        cleanChildList(item.second);
    funcEval.clear();
}


static void callsToMap(const vector<pair<string, int>>& detailCallsFrom, map<int, vector<string>>& funcsMap)
{
    for (const auto& item : detailCallsFrom)
        funcsMap[item.second].push_back(item.first);
}


// passes through all expressions of the operator, calculating its evaluation = val->T for static
// or T for gcov
static void goThroughExpressions(SgExpression* expr, double& T)
{
    if (expr != nullptr)
    {
        switch (expr->variant())
        {
        case ADD_OP:
            T += operationCost.add_op;
            break;
        case MULT_OP:
            T += operationCost.mult_op;
            break;
        case SUBT_OP:
            T += operationCost.subt_op;
            break;
        case EXP_OP:
            T += operationCost.exp_op;
        case VAR_REF:
        case ARRAY_REF:
        case RECORD_REF:
        case ENUM_REF:
            T += operationCost.ref_op;
            break;
        default:
            T += operationCost.base_cost;
            break;
        }
        SgExpression* lhs = expr->lhs();
        SgExpression* rhs = expr->rhs();
        if (lhs)
            goThroughExpressions(lhs, T);
        if (rhs)
            goThroughExpressions(rhs, T);
    }
}



//////// Static /////////


////////////////
// first step //
////////////////


// passes through all the operators of the function, making up a tree of operators' CodeEval
static void goThroughFunction(SgStatement* st, CodeEval* val)
{
    if (val == nullptr || st == nullptr)
        return;
    val->variant = st->variant();
    val->lineNum = st->lineNumber();
    if (st->variant() == STMTFN_STAT) // or STMTFN_DECL ???
        return;
    for (int i = 0; i < st->numberOfChildrenList1(); i++)
    {
        if (st->childList1(i)->variant() != CONTROL_END) {
            CodeEval* newVal = nullptr;
            if (st->childList1(i)->variant() == IF_NODE)
                newVal = new IfEval();
            else
                newVal = new CodeEval();
            goThroughFunction(st->childList1(i), newVal);
            val->childrenList.push_back(newVal);
        }
    }
    if (st->variant() == IF_NODE || st->variant() == ELSEIF_NODE)
    {
        SgIfStmt* iSt = (SgIfStmt*)st;
        IfEval* iVal = (IfEval*)val;
        if (iSt->falseBody() != nullptr && iSt->falseBody()->variant() == ELSEIF_NODE)
        {
            iVal->falseBody = new IfEval();
            goThroughFunction(iSt->falseBody(), iVal->falseBody);
        }
    }
    for (int i = 0; i < 3; i++)
        goThroughExpressions(st->expr(i), val->T);
}



void oneFileEvaluation(SgFile* file, const map<string, vector<FuncInfo*>>& allFuncInfo)
{
    int funcNum = file->numberOfFunctions();
    const vector<FuncInfo*>& funcs = allFuncInfo.at(file->filename());
    for (int i = 0; i < funcNum; i++)
    {
        string fn = funcs[i]->funcName;
        funcEval[fn] = new CodeEval;
        goThroughFunction(file->functions(i), funcEval[fn]);
    }
}


/////////////////
// second step //
/////////////////


// completes the calculation of the operator's score (val->T), taking into account the type of operator and the scores of its children
static void calculateEval(CodeEval* val, const map<int, LoopGraph*>& loopsByLine, const map<int, vector<string>>& callsFrom)
{
    if (val == nullptr)
        return;
    auto callsIt = callsFrom.find(val->lineNum);
    if (callsIt != callsFrom.end()) {
        for (size_t i = 0; i < (*callsIt).second.size(); i++)
        {
            if (funcEval.find((*callsIt).second[i]) != funcEval.end())
                val->T += funcEval[(*callsIt).second[i]]->T;
            else
                val->T += operationCost.call_cost;
        }
    }
    vector<CodeEval*> chList = val->childrenList;
    int chNum = chList.size();
    for (int i = 0; i < chNum; i++)
        calculateEval(chList[i], loopsByLine, callsFrom);
    int var = val->variant;
    if (var == IF_NODE || var == ELSEIF_NODE)
    {
        IfEval* iVal = (IfEval*)val;
        for (int i = 0; i < chNum; i++)
            iVal->T += chList[i]->T;
        if (iVal->falseBody != nullptr)
        {
            calculateEval(iVal->falseBody, loopsByLine, callsFrom);
            if (iVal->T < iVal->falseBody->T)
                iVal->T = iVal->falseBody->T;
        }
    }
    else if (var == FOR_NODE || var == WHILE_NODE)
    {
        int numOfIters = DEFAULT_ITERATIONS;
        if (loopsByLine.find(val->lineNum) != loopsByLine.end() && loopsByLine.at(val->lineNum)->countOfIters > 0)
            numOfIters = loopsByLine.at(val->lineNum)->countOfIters;
        double sum = 0;
        for (int i = 0; i < chNum; i++) {
            sum += chList[i]->T;
        }
        val->T += sum * numOfIters;
        if (loopsByLine.find(val->lineNum) != loopsByLine.end())
            loopsByLine.at(val->lineNum)->executionTimeInSec = val->T;   // save final loop evaluation (with iters)
    }
    else if (var == SWITCH_NODE)
    {
        double curT = 0, finalT = 0;
        for (int i = 0; i < chNum; i++)
        {
            if (chList[i]->variant == CASE_NODE || chList[i]->variant == DEFAULT_NODE)
            {
                if (curT > finalT)
                    finalT = curT;
                curT = 0;
            }
            else
                curT += chList[i]->T;
        }
        if (finalT < curT)
            finalT = curT;
        val->T += finalT;
    }
    else
    {
        for (int i = 0; i < chNum; i++)
            val->T += chList[i]->T;
    }
}


// Going through all the functions of the file, calls the calculateEval
static void goThroughFunctions(FuncInfo* f, const map<string, map<int, LoopGraph*>>& LoopsByLine)
{
    if (f == nullptr)
        return;
    map<string, bool> evaluated; // funcname -> was evaluated?
    map<int, vector<string>> callsFrom;
    callsToMap(f->detailCallsFrom, callsFrom);
    string fileName = f->fileName;
    string funcName = f->funcName;
    for (FuncInfo* call : f->callsFromV)
    {
        if (!call->doNotAnalyze && !evaluated[call->funcName])
            goThroughFunctions(call, LoopsByLine);
    }
    calculateEval(funcEval[funcName], LoopsByLine.at(fileName), callsFrom);
    evaluated[f->funcName] = true;
}


void allFilesEvaluation(const map<string, vector<FuncInfo*>>& allFuncInfo, const map<string, vector<LoopGraph*>>& loopGraph)
{
    map<string, map<int, LoopGraph*>> loopsByLine;
    loopsToMap(loopGraph, loopsByLine);
    FuncInfo* main = findMain(allFuncInfo);
    //
    if (main != nullptr)
    {
        goThroughFunctions(main, loopsByLine);
        cleanFuncEval();
    }
    else
        __spf_print(1, "main not found\n");
}


///////// Gcov /////////


////////////////
// first step //
////////////////


// calculates eval of each line of the file without taking calls into account
void oneFileGcovEvaluation(SgFile* file)
{
    SgStatement* st = file->firstStatement();
    while (st != nullptr)
    {
        double T = 0;
        for (int i = 0; i < 3; i++)
            goThroughExpressions(st->expr(i), T);
        lineEval[st->fileName()][st->lineNumber()] = T;
        st = st->lexNext();
    }
}


/////////////////
// second step //
/////////////////


// calculates eval of the function taking calls into account
static double finalFuncEval(FuncInfo* f, const map<int, Gcov_info >& gLineInfo)
{
    double T = 0;
    pair<int, int> linesNum = f->linesNum;
    map<int, vector<string>> calls;
    callsToMap(f->detailCallsFrom, calls);
    auto fileLinesEvalIt = lineEval.find(f->fileName);
    if (fileLinesEvalIt == lineEval.end())
        return T;
    for (int line = linesNum.first; line <= linesNum.second; line++)
    {
        map<int, double>::iterator lineIt = (*fileLinesEvalIt).second.find(line);
        if (lineIt == (*fileLinesEvalIt).second.end())
            continue;
        auto it = gLineInfo.find(line);
        if (it != gLineInfo.end() && it->second.getExecutedCount() > 0) {
            double curT = 0;
            curT += lineIt->second;
            for (auto func : calls[line])
            {
                if (funcsEval.find(func) != funcsEval.end())
                    curT += funcsEval[func];
                else
                    curT += operationCost.call_cost; // CALL_COST;
            }
            lineIt->second = curT; // add func-eval to line-eval for calculateLoopsInOneFile
            curT *= it->second.getExecutedCount();
            T += curT;
        }
        else
            lineIt->second = 0;
    }
    int callsCnt = 1;
    auto it = gLineInfo.find(linesNum.first);
    if (it != gLineInfo.end())
        callsCnt = it->second.getFunctionCall().getTimes();
    if (callsCnt != 0)
        T /= (double)callsCnt;
    return T;
}


static void calculateFuncs(FuncInfo* f, const map<string, map<int, Gcov_info>>& gCovInfo)
{
    map<string, bool> isEvaluated; // funcname -> was evaluated?
    for (auto& calledFunc : f->callsFromV)
    {
        if (!calledFunc->doNotAnalyze && !isEvaluated[calledFunc->funcName])
            calculateFuncs(calledFunc, gCovInfo);
    }
    funcsEval[f->funcName] = finalFuncEval(f, gCovInfo.at(f->fileName));
}


static void calculateLoops(const map<string, vector<LoopGraph*>>& loopGraph, map<string, map<int, LoopGraph*>>& loopsByLine)
{
    for (const auto& item : loopGraph)
        calculateLoopsInOneFile(item.second, loopsByLine[item.first]);
}


static void calculateLoopsInOneFile(const vector<LoopGraph*>& loopGraphInfo, map<int, LoopGraph*>& loopsByLine)
{
    for (const auto& loop : loopGraphInfo)
    {
        calculateLoopsInOneFile(loop->children, loopsByLine);
        int startLine = loop->lineNum;
        int endLine = loop->lineNumAfterLoop;
        auto startLineIt = loopsByLine.find(startLine);
        if (startLineIt == loopsByLine.end())
            break;
        startLineIt->second->executionTimeInSecGcov = lineEval[loop->fileName][startLine];
        int line = startLine + 1;
        while (line < endLine)
        {
            if (loopsByLine.find(line) != loopsByLine.end())
            {
                startLineIt->second->executionTimeInSecGcov += loopsByLine[line]->executionTimeInSecGcov;
                line = loopsByLine[line]->lineNumAfterLoop;
            }
            else
            {
                startLineIt->second->executionTimeInSecGcov += lineEval[loop->fileName][line];
                line++;
            }
        }
        startLineIt->second->executionTimeInSecGcov *= startLineIt->second->countOfIters;
    }
}


void allFilesGcovEvaluation(const map<string, vector<FuncInfo*>>& allFuncInfo,
    const map<string, vector<LoopGraph*>>& loopGraph, const map<string, map<int, Gcov_info>>& gCovInfo)
{
    map<string, map<int, LoopGraph*>> loopsByLine;
    loopsToMap(loopGraph, loopsByLine);
    FuncInfo* main = findMain(allFuncInfo);
    //
    calculateFuncs(main, gCovInfo);
    calculateLoops(loopGraph, loopsByLine);
}




///////// print /////////

void printLoopsEval(const map<string, vector<LoopGraph*>>& loopGraph) // for check
{
    for (auto item : loopGraph)
    {
        __spf_print(1, "Loops in %s :\n", item.first.c_str());
        printLoopsEvalFromOneFile(item.second);
    }
}

static void printLoopsEvalFromOneFile(vector<LoopGraph*>& loops)
{
    for (auto loop : loops)
    {
        __spf_print(1, "  line = %d :  eval = %lf, gcov_eval = %lf\n",
            loop->lineNum, loop->executionTimeInSec, loop->executionTimeInSecGcov);
        printLoopsEvalFromOneFile(loop->children);
    }
}
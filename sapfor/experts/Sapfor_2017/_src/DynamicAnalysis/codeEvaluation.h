#pragma once

#include <map>
#include <vector>
#include <set>
#include <string>

#include "dvm.h"
#include "../Utils/errors.h"
#include "../GraphCall/graph_calls.h"
#include "../DynamicAnalysis/gcov_info.h"


struct OperationCost {
    const double add_op = 7e-10;
    const double subt_op = 7e-10;
    const double mult_op = 1e-7;
    const double div_op = 1.5e-9;
    const double ref_op = 1.7e-11;
    const double exp_op = 4e-7;
    const double call_cost = 15e-8; // FUNC_CALL, PROC_CALL
    const double base_cost = 0;
};


struct CodeEval
{
    std::vector<CodeEval*> childrenList;
    double T; // score
    int variant; // variant of operator
    int lineNum;
    CodeEval() : T(0), variant(0), lineNum(-1) {}
    CodeEval(const CodeEval& rhs) : childrenList(rhs.childrenList),
        T(rhs.T), variant(rhs.variant), lineNum(rhs.lineNum) {}
    CodeEval& operator=(const CodeEval& rhs) {
        if (this != &rhs) {
            childrenList = rhs.childrenList;
            variant = rhs.variant;
            T = rhs.T;
            lineNum = rhs.lineNum;
        }
        return *this;
    }
};

struct IfEval : CodeEval // for IF and ELSEIF
{
    CodeEval* falseBody;
    IfEval() : CodeEval(), falseBody(nullptr) {}
};


// Static
void allFilesEvaluation(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo, const std::map<std::string, std::vector<LoopGraph*>>& loopGraph);
void oneFileEvaluation(SgFile* file, const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo);
// Gcov
void allFilesGcovEvaluation(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo, const std::map<std::string, std::vector<LoopGraph*>>& loopGraph, const std::map<std::string, std::map<int, Gcov_info>>& gCovInfo);
void oneFileGcovEvaluation(SgFile* file);

////
void printLoopsEval(const std::map<std::string, std::vector<LoopGraph*>>& loopGraph);
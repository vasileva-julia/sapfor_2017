#pragma once
#include <string>
#include <map>
#include <set>
#include <vector>
#include <algorithm>

#include "CFGraph.h"
#include "../Utils/CommonBlock.h"

namespace SAPFOR
{
    enum class CFG_OP { NONE, ASSIGN, LOAD, STORE, REF, PARAM,
                        ADD, MULT, DIV, SUBT, UN_ADD, UN_MINUS, POW,
                        JUMP, JUMP_IF,
                        GE, LE, GT, LT, EQ, NEQV, EQV, EMPTY, OR, AND, NOT,
                        F_CALL, EXIT };

    enum class CFG_ARG_TYPE { NONE, REG, VAR, CONST, FUNC, LAB, INSTR };

    static std::vector<std::string> CFG_OP_S = { "--", " = ", "LOAD ", "STORE ", "REF ", "PARAM ",
                                                 " + ", " * ", " / ", " - ", " + ", "-", " ** ",
                                                 " ", " ", 
                                                 " >= ", " <= ", " > " , " < ", " == ", " != ", " eqv ", "", " || ", " && ", " ! ",
                                                 "F_CALL ", "EXIT ", };

    class Argument
    {
        static int lastNumArg;
    private:
        int number;

        CFG_ARG_TYPE type;
        std::string value;        
    public:
        Argument() : number(lastNumArg++), type(CFG_ARG_TYPE::NONE), value("") { }
        Argument(CFG_ARG_TYPE type) : number(lastNumArg++), type(type), value("") { }
        Argument(CFG_ARG_TYPE type, const std::string& value) : number(lastNumArg++), type(type), value(value) { }

        void setType(CFG_ARG_TYPE newType) { type = newType; }
        CFG_ARG_TYPE getType() const { return type; }

        void setValue(const std::string& newValue) { value = newValue; }
        const std::string& getValue() const { return value; }

        int getNumber() const { return number; }
    };

    class Instruction
    {
        static int lastNumInstr;
    private:
        int number;

        CFG_OP operation;
        Argument* arg1;
        Argument* arg2;
        Argument* result;

        SgStatement* st = NULL;
    public:

        Instruction() : number(lastNumInstr++), operation(CFG_OP::NONE), arg1(NULL), arg2(NULL), result(NULL) { }           
        Instruction(CFG_OP op, Argument* arg1 = NULL, Argument* arg2 = NULL, Argument* res = NULL, SgStatement* st = NULL) :
                    number(lastNumInstr++), operation(op), arg1(arg1), arg2(arg2), result(res), st(st) { }

        Instruction(CFG_OP op, SgStatement* st) : number(lastNumInstr++), operation(op), arg1(NULL), arg2(NULL), result(NULL), st(st) { }

        void setOperation(CFG_OP op) { operation = op; }
        CFG_OP getOperation() const { return operation; }

        Argument* getArg1() const { return arg1; }
        Argument* getArg2() const { return arg2; }
        Argument* getResult() const { return result; }

        void setArg1(Argument* arg) { arg1 = arg; }
        void setArg2(Argument* arg) { arg2 = arg; }
        void setResult(Argument* arg) { result = arg; }

        int getNumber() const { return number; }

        SgStatement* getOperator() const { return st; }
        void setOperator(SgStatement* st_) { st = st_; }

        static int getNextInstrNum() { return lastNumInstr; }

        std::string dump()
        {
            std::string res = "";
            switch (operation)
            {
            case CFG_OP::ADD:
            case CFG_OP::MULT:
            case CFG_OP::DIV:
            case CFG_OP::SUBT:
            case CFG_OP::GE:
            case CFG_OP::LE:
            case CFG_OP::GT:
            case CFG_OP::LT:
            case CFG_OP::EQ:
            case CFG_OP::NEQV:
            case CFG_OP::EQV:
            case CFG_OP::OR:
            case CFG_OP::AND:
            case CFG_OP::POW:
                res = result->getValue() + " = " + arg1->getValue() + CFG_OP_S[(int)operation] + arg2->getValue();
                break;
            case CFG_OP::NOT:
            case CFG_OP::UN_MINUS:
            case CFG_OP::UN_ADD:
                res = result->getValue() + " = " + CFG_OP_S[(int)operation] + arg1->getValue();
                break;
            case CFG_OP::ASSIGN:
                res = result->getValue() + CFG_OP_S[(int)operation] + arg1->getValue();
                break;
            case CFG_OP::JUMP:
                res = "goto " + arg1->getValue();
                break;
            case CFG_OP::JUMP_IF:
                res = "if_false " + arg1->getValue() + " then goto " + arg2->getValue();
                break;
            case CFG_OP::EMPTY:
                res = "continue";
                break;
            case CFG_OP::LOAD:
                res = CFG_OP_S[(int)operation] + result->getValue() + " " + arg1->getValue() + (arg2 ? (" " + arg2->getValue()) : "");
                break;
            case CFG_OP::STORE:
                res = CFG_OP_S[(int)operation] + arg1->getValue() + (arg2 ? (" " + arg2->getValue()) : "") + " " + result->getValue();
                break;
            case CFG_OP::REF:
            case CFG_OP::PARAM:
                res = CFG_OP_S[(int)operation] + arg1->getValue();
                break;
            case CFG_OP::F_CALL:
                if (result)
                    res = result->getValue() + " = " + CFG_OP_S[(int)operation] + arg1->getValue() + " " + arg2->getValue();
                else
                    res = CFG_OP_S[(int)operation] + arg1->getValue() + " " + arg2->getValue();
                break;
            default:
                res = CFG_OP_S[(int)operation];
                break;
            }

            return res;
        }
    };

    class IR_Block
    {
    private:
        Instruction* current;
        Instruction* jumpTo;

        BasicBlock* bblock;
    public:
        IR_Block(Instruction* instr) : current(instr), jumpTo(NULL), bblock(NULL) { }

        Instruction* getInstruction() const { return current; }
        Instruction* getJump() const { return jumpTo; }
        BasicBlock* getBasicBlock() const { return bblock; }

        void addToJump(Instruction* instr) { jumpTo = instr; }
        void setBasicBlock(BasicBlock* newBlock) { bblock = newBlock; }

        ~IR_Block() { delete current; }
    };
}

std::vector<SAPFOR::IR_Block*> buildIR(SgFile* file, const std::map<std::string, CommonBlock*>& commonBlocks, const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo);
#include "../Utils/leak_detector.h"

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include "../Utils/SgUtils.h"
#include "../Utils/CommonBlock.h"
#include "../GraphCall/graph_calls.h"

#include "dvm.h"
#include "IR.h"
#include "CFGraph.h"

using namespace std;
using namespace SAPFOR;

static map<string, SAPFOR::Argument*> dictArgs;
static int lastNumReg = 0;

int Argument::lastNumArg = 0;
int Instruction::lastNumInstr = 0;

static SAPFOR::Argument* processExpression(SgExpression* ex, vector<IR_Block*>& blocks, const FuncInfo* func, 
                                           const vector<pair<const Variable*, CommonBlock*>>& commonVars, SAPFOR::Argument *isLeft = NULL);

static SAPFOR::Argument* createRegister()
{
    const string newName = "_reg" + to_string(lastNumReg++);
    auto it = dictArgs.find(newName);
    if (it == dictArgs.end())
        it = dictArgs.insert(it, make_pair(newName, new SAPFOR::Argument(CFG_ARG_TYPE::REG, newName)));
    return it->second;
}

static string createName(const vector<pair<const Variable*, CommonBlock*>>& commonVars, SgSymbol* s, SgStatement* scope)
{
    string newName = "";
    SgSymbol* var = OriginalSymbol(s);

    bool inCommon = false;
    if (commonVars.size() && var == s)
    {
        const string name = s->identifier();
        for (auto& var : commonVars)
        {
            if (var.first->getName() == name)
            {
                const int pos = var.first->getPosition();
                auto groupedByPos = var.second->getGroupedVars();
                if (groupedByPos.count(pos) == 0)
                    printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

                string firstSynonim = groupedByPos[pos][0]->getName();

                newName = string("_") + var.second->getName() + string("%") + firstSynonim;
                inCommon = true;
                break;
            }
        }
    }
    
    if (!inCommon)
    {        
        if (scope)
            newName = string("_") + scope->symbol()->identifier() + string("%") + var->identifier();
        else
            newName = string("_") + var->identifier();
    }

    return newName;
}

static SAPFOR::Argument* createArg(SgSymbol* var, const vector<pair<const Variable*, CommonBlock*>>& commonVars)
{
    SgStatement* where = OriginalSymbol(var)->scope();
    if (!where)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    const string newName = createName(commonVars, var, where);
    auto it = dictArgs.find(newName);
    if (it == dictArgs.end())
        it = dictArgs.insert(it, make_pair(newName, new SAPFOR::Argument(CFG_ARG_TYPE::VAR, newName)));

    return it->second;    
}

static SAPFOR::Argument* createArrayArg(SgExpression* array_ref, vector<IR_Block*>& blocks, const FuncInfo* func, int& numArgs,
                                        const vector<pair<const Variable*, CommonBlock*>>& commonVars)
{
    auto ref = isSgArrayRefExp(array_ref);
    if (ref == NULL)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    SgSymbol* var = ref->symbol();
    SgStatement* where = OriginalSymbol(var)->scope();
    if (!where)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    vector<SAPFOR::Argument*> args;
    numArgs = ref->numberOfSubscripts();
    for (int z = 0; z < numArgs; ++z)
        args.push_back(processExpression(ref->subscript(z), blocks, func, commonVars));

    for (auto& arg : args)
    {
        Instruction* instr = new Instruction(CFG_OP::REF, arg, NULL, NULL);
        blocks.push_back(new IR_Block(instr));
    }

    const string newName = createName(commonVars, var, where);
    auto it = dictArgs.find(newName);
    if (it == dictArgs.end())
        it = dictArgs.insert(it, make_pair(newName, new SAPFOR::Argument(CFG_ARG_TYPE::VAR, newName)));

    return it->second;
}

static SAPFOR::Argument* createConstArg(SgExpression* exp)
{
    string newName = "---";

    auto value = isSgValueExp(exp);
    int var = exp->variant();
    if (var == INT_VAL)
        newName = to_string(value->intValue());
    else if (var == FLOAT_VAL)
        newName = value->floatValue();
    else if (var == DOUBLE_VAL)
        newName = value->doubleValue();
    else if (var == CHAR_VAL)
        newName = value->charValue();
    else if (var == COMPLEX_VAL)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__); // TODO
    else if (var == BOOL_VAL)
        newName = value->boolValue() ? "TRUE" : "FALSE";
    else 
        newName = value->stringValue();

    auto it = dictArgs.find(newName);
    if (it == dictArgs.end())
        it = dictArgs.insert(it, make_pair(newName, new SAPFOR::Argument(CFG_ARG_TYPE::CONST, newName)));

    return it->second;
}

static SAPFOR::Argument* createConstArg(int value)
{
    string s = to_string(value);

    auto itS = dictArgs.find(s);
    if (itS == dictArgs.end())
        itS = dictArgs.insert(itS, make_pair(s, new SAPFOR::Argument(CFG_ARG_TYPE::CONST, s)));

    return itS->second;
}

template<typename funcStatement>
static void processArgs(funcStatement* call, int num, vector<IR_Block*>& blocks, const FuncInfo* func,
                        const vector<pair<const Variable*, CommonBlock*>>& commonVars)
{
    vector<SAPFOR::Argument*> args;
    for (int z = 0; z < num; ++z)
        args.push_back(processExpression(call->arg(z), blocks, func, commonVars));

    for (auto& arg : args)
    {
        Instruction* instr = new Instruction(CFG_OP::PARAM, arg);
        blocks.push_back(new IR_Block(instr));
    }
}

static SAPFOR::Argument* getFuncArg(const string& fName)
{
    auto it = dictArgs.find(fName);
    if (it == dictArgs.end())
        it = dictArgs.insert(it, make_pair(fName, new SAPFOR::Argument(CFG_ARG_TYPE::FUNC, fName)));
    return it->second;
}

static SAPFOR::Argument* processExpression(SgExpression* ex, vector<IR_Block*>& blocks, const FuncInfo* func, 
                                           const vector<pair<const Variable*, CommonBlock*>>& commonVars, SAPFOR::Argument* isLeft)
{
    if (ex)
    {
        static map<int, CFG_OP> typeMap = { { ADD_OP, CFG_OP::ADD }, 
                                            { MULT_OP, CFG_OP::MULT },
                                            { SUBT_OP, CFG_OP::SUBT },
                                            { DIV_OP, CFG_OP::DIV },
                                            { UNARY_ADD_OP, CFG_OP::UN_ADD },
                                            { MINUS_OP, CFG_OP::UN_MINUS },
                                            { GT_OP, CFG_OP::GT },
                                            { GE_OP, CFG_OP::GE },
                                            { GTEQL_OP, CFG_OP::GE },
                                            { LT_OP, CFG_OP::LT },
                                            { LE_OP, CFG_OP::LE },
                                            { LTEQL_OP, CFG_OP::LE },
                                            { OR_OP, CFG_OP::OR },
                                            { AND_OP, CFG_OP::AND }, 
                                            { EQ_OP, CFG_OP::EQ }, 
                                            { EQV_OP, CFG_OP::EQV }, 
                                            { NEQV_OP, CFG_OP::NEQV }, 
                                            { NOTEQL_OP, CFG_OP::NEQV },
                                            { NOT_OP, CFG_OP::NOT },
                                            { FUNC_CALL, CFG_OP::F_CALL }, 
                                            { EXP_OP, CFG_OP::POW }, };

        const int var = ex->variant();
        if ((var == VAR_REF || var == CONST_REF) && !ex->lhs() && !ex->rhs()) // ��������� � ����������
            return createArg(ex->symbol(), commonVars);
        else if (isSgValueExp(ex))
            return createConstArg(ex);
        else if (var == ADD_OP || var == MULT_OP || var == SUBT_OP || var == DIV_OP ||
                 var == GT_OP || var == GE_OP || var == LT_OP || var == LE_OP ||
                 var == OR_OP || var == AND_OP || var == EQ_OP || var == EQV_OP ||
                 var == NEQV_OP || var == EXP_OP || var == NOTEQL_OP || var == LTEQL_OP ||
                 var == GTEQL_OP)
        {
            auto arg1 = processExpression(ex->lhs(), blocks, func, commonVars);
            auto arg2 = processExpression(ex->rhs(), blocks, func, commonVars);
            auto reg = createRegister();
            Instruction* instr = new Instruction(typeMap[var], arg1, arg2, reg);
            blocks.push_back(new IR_Block(instr));

            return reg;
        }
        else if (var == MINUS_OP || var == UNARY_ADD_OP || var == NOT_OP)
        {
            auto arg1 = processExpression(ex->lhs(), blocks, func, commonVars);
            auto reg = createRegister();
            Instruction* instr = new Instruction(typeMap[var], arg1, NULL, reg);
            blocks.push_back(new IR_Block(instr));

            return reg;
        }
        else if (var == ARRAY_REF)
        {
            int numArgs = 0;
            auto arg1 = createArrayArg(ex, blocks, func, numArgs, commonVars);
            
            if (numArgs == 0)
                return arg1;

            auto reg = isLeft ? NULL : createRegister();
            Instruction* instr = new Instruction(isLeft ? CFG_OP::STORE : CFG_OP::LOAD, arg1, createConstArg(numArgs), isLeft ? isLeft : reg);
            blocks.push_back(new IR_Block(instr));
            return reg;
        }
        else if (var == FUNC_CALL)
        {
            auto fCall = isSgFunctionCallExp(ex);
            processArgs(fCall, fCall->numberOfArgs(), blocks, func, commonVars);

            SAPFOR::Argument* fArg = getFuncArg(ex->symbol()->identifier());

            auto reg = createRegister();
            Instruction* instr = new Instruction(typeMap[var], fArg, createConstArg(fCall->numberOfArgs()), reg);
            blocks.push_back(new IR_Block(instr));
            return reg;
        }
        else
        {
            __spf_print(1, "unknown expression '%s'\n", tag[ex->variant()]);
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
        }
    }

    printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    return NULL;
}

static SgStatement* processLabel(SgStatement* st, const int firstInstr, const vector<IR_Block*>& blocks, map<int, Instruction*>& labels)
{
    const int var = st->variant();
    if (var == FORMAT_STAT)
        return st;

    if (st->label())
    {
        int lab = st->label()->getLabNumber();
        labels[lab] = blocks[firstInstr]->getInstruction();
    }
    return st;
}

static SgStatement* getNearestDo(SgStatement* st)
{
    while (st->variant() != FOR_NODE && st->variant() != WHILE_NODE)
    {        
        st = st->controlParent();
        if (st == NULL)
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    }
    return st;
}

static void findCycle(int start, int end, const vector<IR_Block*>& blocks, SgStatement* curr, int jumpTo)
{
    for (int z = start; z < end; ++z)
    {
        if (blocks[z]->getInstruction()->getOperation() == CFG_OP::JUMP)
            if (blocks[z]->getInstruction()->getOperator())
                if (blocks[z]->getInstruction()->getOperator()->variant() == CYCLE_STMT)
                    if (getNearestDo(blocks[z]->getInstruction()->getOperator()) == curr)
                        blocks[z]->getInstruction()->setArg1(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(jumpTo)));
    }
}

static void findReturn(int start, int end, const vector<IR_Block*>& blocks, int jumpTo)
{
    for (int z = start; z < end; ++z)
    {
        if (blocks[z]->getInstruction()->getOperation() == CFG_OP::JUMP)
            if (blocks[z]->getInstruction()->getOperator())
                if (blocks[z]->getInstruction()->getOperator()->variant() == RETURN_STAT)
                    blocks[z]->getInstruction()->setArg1(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(jumpTo)));
    }
}

static void addGotoTo(int labNum, vector<IR_Block*>& blocks, SgStatement* st = NULL)
{
    auto arg1 = new SAPFOR::Argument(CFG_ARG_TYPE::LAB, to_string(labNum));
    blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP, arg1, NULL, NULL, st)));
}

static SgStatement* processStatement(SgStatement* st, vector<IR_Block*>& blocks, map<int, Instruction*>& labels, const FuncInfo* func,
                                     const vector<pair<const Variable*, CommonBlock*>>& commonVars)
{
    const int var = st->variant();
    //printf("%d %d\n", st->lineNumber(), st->variant());

    static const set<int> skip = { FORMAT_STAT, CONTROL_END, CONT_STAT,
                                   OPEN_STAT, CLOSE_STAT // <--- TODO
                                  };

    const int firstBlock = blocks.size();

    if (var == ASSIGN_STAT)
    {
        auto arg1 = processExpression(st->expr(1), blocks, func, commonVars);
        auto res = processExpression(st->expr(0), blocks, func, commonVars, arg1);

        if (res)
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, arg1, NULL, res)));

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        processLabel(st, firstBlock, blocks, labels);
    }
    else if (var == GOTO_NODE)
    {
        addGotoTo(((SgGotoStmt*)st)->branchLabel()->getLabNumber(), blocks, st);
        processLabel(st, firstBlock, blocks, labels);
    }
    else if (var == COMGOTO_NODE)
    {
        auto comGoto = isSgComputedGotoStmt(st);
        
        auto regCond = processExpression(comGoto->exp(), blocks, func, commonVars);
        
        int num = comGoto->numberOfTargets();
        SgExpression* lab = comGoto->labelList();

        for (int z = 0; z < num; ++z, lab = lab->rhs())
        {
            SgLabel* lbl = ((SgLabelRefExp*)(lab->lhs()))->label();
            auto reg = createRegister();
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::EQ, regCond, createConstArg(z + 1), reg)));
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, reg, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.size() + 2)) )));

            addGotoTo(lbl->getLabNumber(), blocks, st);
        }

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);
        processLabel(st, firstBlock, blocks, labels);
    }
    else if (var == IF_NODE || var == ELSEIF_NODE)
    {
        auto ifSt = (SgIfStmt*)(st);
        auto last = st->lastNodeOfStmt();
        for (SgStatement* s = st; s != st->lastNodeOfStmt(); s = s->lexNext())
        {
            if (s->variant() == ELSEIF_NODE && s != st)
            {
                last = s->lexPrev();
                break;
            }
        }
        if (ifSt->falseBody())
            last = ifSt->falseBody()->lexPrev();

        auto regCond = processExpression(ifSt->conditional(), blocks, func, commonVars);
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, regCond)));
        int jump_if = blocks.size() - 1;

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);
        SgStatement* s = st;
        do
        {
            s = s->lexNext();
            const int firstInstr = blocks.size();
            s = processLabel(processStatement(s, blocks, labels, func, commonVars), firstInstr, blocks, labels);
        } while (s != last);

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, last)));
        blocks[jump_if]->getInstruction()->setArg2(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.back()->getInstruction()->getNumber())));

        processLabel(st, firstBlock, blocks, labels);
        st = last;
    }
    else if (var == LOGIF_NODE)
    {
        auto ifLog = isSgLogIfStmt(st);

        auto regCond = processExpression(ifLog->conditional(), blocks, func, commonVars);
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, regCond)));
        int jump_if = blocks.size() - 1;

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);
        
        processLabel(processStatement(ifLog->lexNext(), blocks, labels, func, commonVars), blocks.size(), blocks, labels);

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, st)));
        blocks[jump_if]->getInstruction()->setArg2(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.back()->getInstruction()->getNumber())));

        processLabel(ifLog, firstBlock, blocks, labels);
        st = st->lexNext();
    }
    else if (var == ARITHIF_NODE)
    {
        SgExpression* cond = st->expr(0);
        SgExpression* lb = st->expr(1);
        SgLabel* arith_lab[3];

        int i = 0;
        while (lb)
        {
            SgLabel* lab = ((SgLabelRefExp*)(lb->lhs()))->label();
            arith_lab[i] = lab;
            i++;
            lb = lb->rhs();
        }
        
        auto regCond = processExpression(st->expr(0), blocks, func, commonVars);
        auto argZero = createConstArg(0);

        if (arith_lab[1]->getLabNumber() == arith_lab[2]->getLabNumber())
        {            
            auto reg = createRegister();
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::LT, regCond, argZero, reg)));
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, reg, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.size() + 2)))));

            addGotoTo(arith_lab[0]->getLabNumber(), blocks);
            addGotoTo(arith_lab[1]->getLabNumber(), blocks);
        }
        else if (arith_lab[0]->getLabNumber() == arith_lab[1]->getLabNumber())
        {
            auto reg = createRegister();
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::LE, regCond, argZero, reg)));
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, reg, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.size() + 2)))));

            addGotoTo(arith_lab[1]->getLabNumber(), blocks);
            addGotoTo(arith_lab[2]->getLabNumber(), blocks);
        }
        else
        {
            auto reg = createRegister();
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::LT, regCond, argZero, reg)));
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, reg, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.size() + 2)))));
            addGotoTo(arith_lab[0]->getLabNumber(), blocks);

            auto reg1 = createRegister();
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::EQ, regCond, argZero, reg1)));
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, reg1, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.size() + 2)))));
            addGotoTo(arith_lab[1]->getLabNumber(), blocks);
            addGotoTo(arith_lab[2]->getLabNumber(), blocks);
        }

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        processLabel(st, firstBlock, blocks, labels);
    }
    else if (var == FOR_NODE) // MAX (INT ((end - start + step) / step), 0)
    {        
        SgForStmt* forSt = isSgForStmt(st);
        SgStatement* last = forSt->lastNodeOfStmt();
                
        auto argZero = createConstArg(0);
        auto idxArg = createRegister();
        
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, argZero, NULL, idxArg)));

        auto forArg = createArg(forSt->doName(), commonVars);
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, processExpression(forSt->start(), blocks, func, commonVars), NULL, forArg)));

        auto regEnd = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, processExpression(forSt->end(), blocks, func, commonVars), NULL, regEnd)));

        auto step = forSt->step();
        if (!step)
            step = new SgValueExp(1);

        auto regStep = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, processExpression(step, blocks, func, commonVars), NULL, regStep)));

        auto iters = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::SUBT, regEnd, forArg, iters)));
        auto iters1 = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ADD, iters, regStep, iters1)));
        auto iters2 = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::DIV, iters1, regStep, iters2)));

        auto regCond = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::LT, idxArg, iters2, regCond)));

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, regCond)));
        int jump_if = blocks.back()->getInstruction()->getNumber();
        int jump_if_N = blocks.size() - 1;

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        for (auto stF = forSt->lexNext(); stF != last; stF = stF->lexNext())
        {
            const int firstInstr = blocks.size();
            stF = processLabel(processStatement(stF, blocks, labels, func, commonVars), firstInstr, blocks, labels);
        }

        int gotoNextIter = blocks.back()->getInstruction()->getNumber();
        int gotoNextIter_N = blocks.size();

        int lastNum = blocks.size();
        auto tmpReg = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ADD, forArg, regStep, tmpReg, st)));
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ADD, idxArg, createConstArg(1), idxArg, st)));

        for (int z = lastNum; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, tmpReg, NULL, forArg, st)));
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(jump_if)), NULL, NULL, st)));

        findCycle(jump_if_N, gotoNextIter_N, blocks, st, gotoNextIter);

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, last)));
        blocks[jump_if_N]->getInstruction()->setArg2(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.back()->getInstruction()->getNumber())));

        processLabel(forSt, firstBlock, blocks, labels);
        st = last;
    }
    else if (var == WHILE_NODE)
    {
        SgWhileStmt* forSt = isSgWhileStmt(st);
        SgStatement* last = forSt->lastNodeOfStmt();

        int firstBlock = blocks.size();

        auto regCond = createRegister();
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::ASSIGN, processExpression(forSt->conditional(), blocks, func, commonVars), NULL, regCond)));
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, regCond, NULL, NULL)));

        int jump_if = blocks.back()->getInstruction()->getNumber();
        int jump_if_N = blocks.size() - 1;

        int first = blocks[firstBlock]->getInstruction()->getNumber();

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        for (auto stF = forSt->lexNext(); stF != last; stF = stF->lexNext())
        {
            const int firstInstr = blocks.size();
            stF = processLabel(processStatement(stF, blocks, labels, func, commonVars), firstInstr, blocks, labels);
        }

        findCycle(jump_if_N, blocks.size(), blocks, st, first);
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP, new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(first)), NULL, NULL, st)));

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, last)));
        blocks[jump_if_N]->getInstruction()->setArg2(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.back()->getInstruction()->getNumber())));

        processLabel(forSt, firstBlock, blocks, labels);
        st = last;
    }
    else if (var == EXIT_STMT || var == STOP_STAT)
    {
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::EXIT, st)));
        processLabel(st, firstBlock, blocks, labels);
    }
    else if ((var == CONT_STAT || var == CONTROL_END))
    {
        if (isSgProgHedrStmt(st->controlParent()))
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, st)));
        else if (st->label())
            blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, st)));
    }
    else if (var == RETURN_STAT)
    {
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP, st))); // need to add argument 
        processLabel(st, firstBlock, blocks, labels);
    }
    else if (var == PROC_STAT)
    {    
        auto call = isSgCallStmt(st);
        processArgs(call, call->numberOfArgs(), blocks, func, commonVars);

        string callName = "";
        for (int z = 0; z < func->detailCallsFrom.size(); ++z)
        {
            if (func->detailCallsFrom[z].second == st->lineNumber())
            {
                callName = func->detailCallsFrom[z].first;
                break;
            }
        }

        SAPFOR::Argument* fArg = getFuncArg(callName);

        Instruction* instr = new Instruction(CFG_OP::F_CALL, fArg, createConstArg(call->numberOfArgs()));
        blocks.push_back(new IR_Block(instr));

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        processLabel(call, firstBlock, blocks, labels);
    }
    else if (var == PRINT_STAT || var == WRITE_STAT || var == READ_STAT)
    {
        SgInputOutputStmt* io = isSgInputOutputStmt(st);
        SgExpression* item = io->itemList();

        vector<SAPFOR::Argument*> args;
        while (item)
        {
            args.push_back(processExpression(item->lhs(), blocks, func, commonVars));
            item = item->rhs();
        }

        for (auto& arg : args)
        {
            Instruction* instr = new Instruction(CFG_OP::PARAM, arg);
            blocks.push_back(new IR_Block(instr));
        }

        SAPFOR::Argument* fArg = NULL;

        if (var == PRINT_STAT)
            fArg = getFuncArg("PRINT");
        else if (var == WRITE_STAT)
            fArg = getFuncArg("WRITE");
        else if (var == READ_STAT)
            fArg = getFuncArg("READ");
        else
            printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

        Instruction* instr = new Instruction(CFG_OP::F_CALL, fArg, createConstArg(args.size()));
        blocks.push_back(new IR_Block(instr));

        for (int z = firstBlock; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(st);

        processLabel(io, firstBlock, blocks, labels);
    }
    else if (var == CYCLE_STMT)
    {
        blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP, st))); // need to add argument 
        processLabel(st, firstBlock, blocks, labels);
    }
    else if (var == SWITCH_NODE)
    {
        auto select = isSgSwitchStmt(st);
        SgStatement* lastSelect = select->lastNodeOfStmt();

        int lastNum = blocks.size();
        auto condSelect = processExpression(select->selector(), blocks, func, commonVars);
        for (int z = lastNum; z < blocks.size(); ++z)
            blocks[z]->getInstruction()->setOperator(select);

        vector<int> jumpsToEnd;
        bool hasDef = select->defOption();
        int numOfCases = select->numberOfCaseOptions();
        for (int z = 0; z < numOfCases; ++z)
        {
            auto caseOp = isSgCaseOptionStmt(select->caseOption(z));
            SgExpression* caseCond = caseOp->caseRangeList();
            if (caseCond->variant() == EXPR_LIST)
                caseCond = caseCond->lhs();

            auto regCond = createRegister();
            lastNum = blocks.size();
            if (caseCond->variant() == DDOT)
            {
                SAPFOR::Argument *left = NULL, *right = NULL;
                if (caseCond->lhs())
                    left = processExpression(caseCond->lhs(), blocks, func, commonVars);
                if (caseCond->rhs())
                    right = processExpression(caseCond->rhs(), blocks, func, commonVars);

                if (left && right)
                {
                    auto tmp1 = createRegister();
                    auto tmp2 = createRegister();

                    blocks.push_back(new IR_Block(new Instruction(CFG_OP::GE, condSelect, left, tmp1)));
                    blocks.push_back(new IR_Block(new Instruction(CFG_OP::LE, condSelect, right, tmp2)));
                    blocks.push_back(new IR_Block(new Instruction(CFG_OP::AND, tmp1, tmp2, regCond)));
                }
                else if (left)
                    blocks.push_back(new IR_Block(new Instruction(CFG_OP::GE, condSelect, left, regCond)));
                else
                    blocks.push_back(new IR_Block(new Instruction(CFG_OP::LE, condSelect, right, regCond)));
            }
            else
            {
                auto condCaseReg = processExpression(caseCond, blocks, func, commonVars);
                blocks.push_back(new IR_Block(new Instruction(CFG_OP::EQ, condSelect, condCaseReg, regCond)));
            }

            blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP_IF, regCond)));
            for (int z = lastNum; z < blocks.size(); ++z)
                blocks[z]->getInstruction()->setOperator(caseOp);

            int jump_if = blocks.size() - 1;

            SgStatement* body = caseOp->lexNext();
            while (body != lastSelect &&
                   body->variant() != CASE_NODE &&
                   body->variant() != DEFAULT_NODE)
            {
                const int firstInstr = blocks.size();
                body = processLabel(processStatement(body, blocks, labels, func, commonVars), firstInstr, blocks, labels);

                body = body->lexNext();
            }

            if (hasDef || z != numOfCases - 1)
            {
                blocks.push_back(new IR_Block(new Instruction(CFG_OP::JUMP, NULL, NULL, NULL, caseOp))); // to end
                jumpsToEnd.push_back(blocks.size() - 1);
            }

            blocks[jump_if]->getInstruction()->setArg2(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(Instruction::getNextInstrNum())));
        }

        if (hasDef)
        {
            SgStatement* body = select->defOption()->lexNext();

            while (body != lastSelect)
            {
                const int firstInstr = blocks.size();
                body = processLabel(processStatement(body, blocks, labels, func, commonVars), firstInstr, blocks, labels);

                body = body->lexNext();
            }
        }

        blocks.push_back(new IR_Block(new Instruction(CFG_OP::EMPTY, lastSelect)));

        for (auto& elem : jumpsToEnd)
            blocks[elem]->getInstruction()->setArg1(new SAPFOR::Argument(CFG_ARG_TYPE::INSTR, to_string(blocks.back()->getInstruction()->getNumber())));

        processLabel(st, firstBlock, blocks, labels);
        st = lastSelect;
    }
    else if (skip.find(var) == skip.end())
    {
        __spf_print(1, "unknown statement '%s' on line %d and file %s\n", tag[st->variant()], st->lineNumber(), st->fileName());
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    }
    
    return st;
}

static Instruction* getInstructionByNumber(const vector<IR_Block*>& blocks, int num)
{
    for (auto& elem : blocks)
        if (elem->getInstruction()->getNumber() == num)
            return elem->getInstruction();

    return NULL;
}

static vector<IR_Block*> buildIR(SgStatement* function, const FuncInfo* func, const vector<pair<const Variable*, CommonBlock*>>& commonVars)
{
    vector<IR_Block*> blocks;
    map<int, Instruction*> labels;

    SgStatement* end = function->lastNodeOfStmt();
    SgStatement* st = function;
    do 
    {
        st = st->lexNext();
        if (st->variant() == CONTAINS_STMT)
            break;

        if (isDVM_stat(st) || isSPF_stat(st))
            continue;

        if (!isSgExecutableStatement(st))
            continue;

        const int firstInstr = blocks.size() ? blocks.size() : 1;
        st = processLabel(processStatement(st, blocks, labels, func, commonVars), firstInstr, blocks, labels);
    } while (st != end);

    if (blocks.back()->getInstruction()->getOperator() != end)
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
    else
        findReturn(0, blocks.size(), blocks, blocks.back()->getInstruction()->getNumber());

    // ���������� ������ �� GOTO � ���������
    for (int z = 0; z < blocks.size(); ++z)
    {
        auto op = blocks[z]->getInstruction()->getOperation();
        if (op == CFG_OP::JUMP || op == CFG_OP::JUMP_IF)
        {
            auto arg = (op == CFG_OP::JUMP) ? blocks[z]->getInstruction()->getArg1() : blocks[z]->getInstruction()->getArg2();
            const int lab = std::stoi(arg->getValue());

            if (arg->getType() == CFG_ARG_TYPE::LAB)
            {
                auto it = labels.find(lab);
                if (it == labels.end())
                    printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

                blocks[z]->addToJump(it->second);

                // ������� ����� �� ����� ����������
                arg->setValue(to_string(it->second->getNumber())); 
                arg->setType(CFG_ARG_TYPE::INSTR);
            }
            else if (arg->getType() == CFG_ARG_TYPE::INSTR)
                blocks[z]->addToJump(getInstructionByNumber(blocks, lab));
            else
                printInternalError(convertFileName(__FILE__).c_str(), __LINE__);
        }
    }

    return blocks;
}

static vector<pair<const Variable*, CommonBlock*>> getCommonsByFunction(SgFile* file, SgStatement* function, const map<string, CommonBlock*>& commonBlocks)
{ 
    vector<pair<const Variable*, CommonBlock*>> retVal;

    for (auto& block : commonBlocks)
    {
        auto vars = block.second->getVariables(file, function);
        for (auto&var : vars)
            retVal.push_back(make_pair(var, block.second));
    }
    return retVal;
}

vector<IR_Block*> buildIR(SgFile* file,
                          const map<string, CommonBlock*>& commonBlocks, 
                          const map<string, vector<FuncInfo*>>& allFuncInfo)
{
    auto byFile = allFuncInfo.find(file->filename());
    
    if (byFile == allFuncInfo.end() && file->numberOfFunctions())
        printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

    vector<IR_Block*> allBlocks;
    for (int z = 0; z < file->numberOfFunctions(); ++z)
    {
        SgStatement* function = file->functions(z);

        if (!isSgProgHedrStmt(function))
            continue;

        FuncInfo* currF = NULL;
        for (auto& elem : byFile->second)
        {
            if (elem->linesNum.first == function->lineNumber())
            {
                currF = elem;
                break;
            }
        }

        vector<IR_Block*> blocks = buildIR(function, currF, getCommonsByFunction(file, function, commonBlocks));
        allBlocks.insert(allBlocks.end(), blocks.begin(), blocks.end());

        /*vector<SAPFOR::BasicBlock*> bblocks = buildCFG(blocks);

        SAPFOR::BasicBlock* curr = blocks[0]->getBasicBlock();
        for (auto& block : blocks)
        {
            auto instr = block->getInstruction();
            auto oper = instr->getOperator();

            if (curr != block->getBasicBlock())
            {
                printf(" -> [next BB]: ");
                for (auto& next : curr->getNext())
                    printf("%d ", next->getNumber());
                printf("\n\n");
                curr = block->getBasicBlock();
            }
            printf("[%d] [line %d] [BB %d] %s\n", instr->getNumber(), oper ? oper->lineNumber() : -1, block->getBasicBlock()->getNumber(), instr->dump().c_str());
        }
        printf("\n");*/
    }
    return allBlocks;
}
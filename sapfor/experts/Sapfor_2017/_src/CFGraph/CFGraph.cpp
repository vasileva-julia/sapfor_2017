#define _LEAK_
#include "../Utils/leak_detector.h"

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <set>

#include "../Utils/SgUtils.h"
#include "../Utils/CommonBlock.h"
#include "../GraphCall/graph_calls.h"

#include "dvm.h"
#include "IR.h"
#include "CFGraph.h"


using namespace std;
using namespace SAPFOR;

typedef SAPFOR::BasicBlock BBlock;

int BBlock::lastNumBlock = 0;

BBlock::BasicBlock(IR_Block* item)
{
    num = lastNumBlock++;
    instructions.push_back(item);
    item->setBasicBlock(this);
}

void BBlock::addInstruction(IR_Block* item)
{
    instructions.push_back(item);
    item->setBasicBlock(this);
}

vector<BBlock*> buildCFG(const vector<IR_Block*>& instruction)
{
    vector<BBlock*> bblocks;

    map<IR_Block*, BBlock*> createdBlocks;
    map<Instruction*, IR_Block*> instrToIr;
    map<int, IR_Block*> instrNumToIr;

    for (auto& ir : instruction)
    {
        instrToIr[ir->getInstruction()] = ir;
        instrNumToIr[ir->getInstruction()->getNumber()] = ir;
    }

    for (int z = 0; z < instruction.size(); ++z)
    {
        if (z == 0)
        {
            bblocks.push_back(new BBlock(instruction[z]));
            createdBlocks[instruction[z]] = bblocks.back();
        }
        else
        {
            if (instruction[z]->getInstruction()->getOperation() == CFG_OP::JUMP ||
                instruction[z]->getInstruction()->getOperation() == CFG_OP::JUMP_IF)
            {
                auto irJump = instrToIr[instruction[z]->getJump()];
                if (irJump == NULL)
                    printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

                if (createdBlocks.count(irJump) == 0)
                {
                    bblocks.push_back(new BBlock(irJump));
                    createdBlocks[irJump] = bblocks.back();
                }

                if (z + 1 != instruction.size() - 1)
                {
                    auto irNext = instruction[z + 1];
                    if (createdBlocks.count(irNext) == 0)
                    {
                        bblocks.push_back(new BBlock(irNext));
                        createdBlocks[irNext] = bblocks.back();
                    }
                }
            }
        }
    }

    BBlock* currBlock = NULL;
    for (auto& ir : instruction)
    {
        if (createdBlocks.count(ir) != 0)
            currBlock = createdBlocks[ir];
        else
        {
            if (currBlock == NULL)
                printInternalError(convertFileName(__FILE__).c_str(), __LINE__);

            currBlock->addInstruction(ir);
        }
    }

    for (auto& bblock : bblocks)
    {
        auto lastInstr = bblock->getInstructions().back();

        BBlock* fromJump = NULL;
        if (lastInstr->getJump())
        {
            fromJump = instrToIr[lastInstr->getJump()]->getBasicBlock();
            bblock->addNext(fromJump);
        }

        int next = lastInstr->getInstruction()->getNumber() + 1;
        if (instrNumToIr.count(next) != 0)
        {
            auto toAdd = instrNumToIr[next]->getBasicBlock();
            if (fromJump != toAdd)
                bblock->addNext(toAdd);
        }
    }

    for (auto& bblock : bblocks)
        for (auto& next : bblock->getNext())
            next->addPrev(bblock);

    return bblocks;
}
#pragma once

#include <string>
#include <map>
#include <set>
#include <vector>

#include "IR.h"

namespace SAPFOR
{
    class IR_Block;

    class BasicBlock
    {
        static int lastNumBlock;
    private:
        int num;
        std::vector<IR_Block*> instructions;

        std::vector<BasicBlock*> next;
        std::vector<BasicBlock*> prev;
        
    public:
        BasicBlock() { num = lastNumBlock++; }
        BasicBlock(IR_Block* item);

        void addInstruction(IR_Block* item);
        void addPrev(BasicBlock* prev_) { prev.push_back(prev_); }
        void addNext(BasicBlock* next_) { next.push_back(next_); }
        
        int getNumber() const { return num; }
        const std::vector<IR_Block*>& getInstructions() const { return instructions; }
        const std::vector<BasicBlock*>& getNext() const { return next; }
        const std::vector<BasicBlock*>& getPrev() const { return prev; }
    };
}

std::vector<SAPFOR::BasicBlock*> buildCFG(const std::vector<SAPFOR::IR_Block*>& instruction);